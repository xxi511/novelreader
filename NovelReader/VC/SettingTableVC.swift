//
//  SettingTableVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 26/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit
import XIPalette

class SettingTableVC: UITableViewController {

    @IBOutlet var bgColorBtn: UIButton!
    @IBOutlet var textColorBtn: UIButton!
    @IBOutlet var scrollSW: UISwitch!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var bgColorLabel: UILabel!
    @IBOutlet var textColorLabel: UILabel!
    @IBOutlet var scrollLabel: UILabel!

    var accountTextField: UITextField?
    var passwordTextField: UITextField?
    /// set/get when color setting only
    var isSettingBG = false
    var config = DataBase.getConfig()

    class func instance() -> SettingTableVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: VCNames.settingTableVC.rawValue) as! SettingTableVC
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
        // TODO: 調色盤
        // TODO: 其他設定
        // TODO: realm
        // TODO: 登出
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickBGColorBtn(_ sender: Any) {
        self.isSettingBG = true
        let vc = XIPalettePopVC.instance()
        vc.delegate = self
        vc.present(at: self)
    }
    
    @IBAction func clickTextColorBtn(_ sender: Any) {
        self.isSettingBG = false
        let vc = XIPalettePopVC.instance()
        vc.delegate = self
        vc.present(at: self)
    }
    
    @IBAction func clickDelBtn(_ sender: Any) {
    }
    
    @IBAction func clickLoginBtn(_ sender: UIButton) {
        (sender.titleLabel?.text == "登入卡提諾") ? self.login(): self.logout()
    }

    @IBAction func clickScrollSW(_ sender: UISwitch) {
        DataBase.setPopOption(sender.isOn)
    }

}

// MARK: Network
extension SettingTableVC {
    func login() {
        let alert = UIAlertController(title: "登入卡提諾", message: nil, preferredStyle: .alert)
        alert.addTextField { account in
            account.text = self.accountTextField?.text
            self.accountTextField = account
            self.accountTextField?.placeholder = "請輸入帳號"
        }
        
        alert.addTextField { passowrd in
            passowrd.text = self.passwordTextField?.text
            self.passwordTextField = passowrd
            self.passwordTextField?.isSecureTextEntry = true
            self.passwordTextField?.placeholder = "請輸入密碼"
        }
        
        let login = UIAlertAction(title: "登入", style: .default) { _ in
            guard let account = self.accountTextField?.text,
                let password = self.passwordTextField?.text else {return}
            
            HTTPClient.login(account: account, password: password, completionHandler: { success, msg in
                if success {
                    self.setLogInBtn(isLogin: true)
                    DataBase.setAccount(account, pass: password)
                }
                
                self.showNoticeAlert(title: nil, msg: msg)
            })
            
        }
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(login)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func logout() {
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
        DataBase.setAccount(nil, pass: nil)
        self.setLogInBtn(isLogin: false)
    }
}

// MARK: - Table view data source
extension SettingTableVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
}

// MARK: Funcs
extension SettingTableVC {
    func viewInit() {
        self.title = "設定"
        self.bgColorBtn.layer.borderColor = UIColor(hex: "333333").cgColor
        self.bgColorBtn.layer.borderWidth = 1
        self.bgColorBtn.backgroundColor = UIColor(hex: self.config.bgColor)
        self.textColorBtn.layer.borderColor = UIColor(hex: "333333").cgColor
        self.textColorBtn.layer.borderWidth = 1
        self.textColorBtn.backgroundColor = UIColor(hex: self.config.frontColor)
        self.scrollSW.isOn = self.config.popByEdgePan
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bgColorBtn.backgroundColor = UIColor(hex: config.bgColor)
        self.tableView.backgroundColor = UIColor(hex: config.bgColor)
        self.bgColorLabel.textColor = UIColor(hex: config.frontColor)
        self.textColorLabel.textColor = UIColor(hex: config.frontColor)
        self.scrollLabel.textColor = UIColor(hex: config.frontColor)
        self.textColorBtn.backgroundColor = UIColor(hex: config.frontColor)

        
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            if cookie.name.contains("auth") && cookie.domain == ".ck101.com" {
                self.setLogInBtn(isLogin: true)
                break
            }
        }
    }
    
    func setLogInBtn(isLogin: Bool) {
        let title = isLogin ? "登出": "登入卡提諾"
        let color = isLogin ? UIColor.red: UIColor(hex: "007aff")
        
        DispatchQueue.main.async {
            self.loginBtn.setTitle(title, for: .normal)
            self.loginBtn.tintColor = color
        }
    }
}

extension SettingTableVC: XIPaletteDelegate {
    func select(color: UIColor) {
        if self.isSettingBG {
            self.tableView.backgroundColor = color
            self.bgColorBtn.backgroundColor = color
            UIApplication.shared.setStatusBarBgColor(color: color)
            self.setBarColor(color: color)
            DataBase.setBackgroundColor(color.toHexString())
        } else {
            self.textColorBtn.backgroundColor = color
            self.bgColorLabel.textColor = color
            self.textColorLabel.textColor = color
            self.scrollLabel.textColor = color
            self.setBarItemColor(color: color)
            DataBase.setFrontColor(color.toHexString())
        }
    }
}
