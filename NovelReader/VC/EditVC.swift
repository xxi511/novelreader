//
//  EditVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 19/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit

class EditVC: UIViewController {

    @IBOutlet var content: UITextView!
    @IBOutlet var editDicBtn: UIButton!
    @IBOutlet var modifyBtn: UIButton!
    var modifyData: EditInfo?

    class func instance() -> EditVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: VCNames.editVC.rawValue) as! EditVC
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver(names: [.SendModify])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeNotiObserver()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickEditDicBtn(_ sender: Any) {
        self.setBackBtn()
        let vc = WordDicTableVC.instance()
        self.navigationController?.show(vc, sender: self)
    }
    
    @IBAction func clickModifyBtn(_ sender: Any) {
        self.view.makeToastActivity()
        DispatchQueue.global(qos: .default).async {
            var arranged = self.arrange()
            let wordDic = DataBase.loadWordDic()
            for word in wordDic {
                if arranged.contains(word.strOld) {
                    let new = (word.strNew == "!@#$%") ? "": word.strNew
                    arranged = arranged.replace(word.strOld, to: new)
                }
            }
           
            DispatchQueue.main.async {
                self.content.text = arranged
                self.view.hideToastActivity()
            }
        }

    }
}

// MARK: Network
extension EditVC {
    @objc func sendModified() {
        self.view.makeToastActivity()
        self.modifyData?.message = self.content.text
        HTTPClient.sendModifyData(info: self.modifyData!)
    }
    
    override func update(noti: Notification) {
        DispatchQueue.main.async {
            self.view.hideToastActivity()
            switch noti.name {
            case .SendModify: self.updateContent(res: noti.object as! [String: Any])
            default: return
            }
        }
    }
    
    func updateContent(res: [String: Any]) {
        let success = res["success"] as! Bool
        let errMsg = res["errMsg"] as! String
        if !success {
            self.showNoticeAlert(title: nil, msg: errMsg)
            return
        }
        
        let readVC = self.lastVC() as! NovelReadVC
        readVC.setContent(self.content.text, In: (self.modifyData?.row)!)
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: Funcs
extension EditVC {
    func viewInit() {
        self.content.setBorder(color: UIColor.darkGray.cgColor)
        self.editDicBtn.setCorner(5)
        self.modifyBtn.setCorner(5)
        self.content.text = self.modifyData?.message
        self.title = "編輯"
        
        let item = UIBarButtonItem(title: "送出", style: .plain, target: self, action: #selector(self.sendModified))
        self.navigationItem.rightBarButtonItem = item

        let config = DataBase.getConfig()
        self.view.backgroundColor = UIColor(hex: config.bgColor)
        self.content.textColor = UIColor(hex: config.frontColor)
    }
    
    func arrange() -> String {
        guard let tmp = self.content.text else {return ""}
        var paragraph = tmp.split(separator: "\r\n")
        paragraph = paragraph.enumerated().map({ (idx, para) -> Substring in
            var trim = para.trimmingCharacters(in: .whitespaces)
            if idx != 0 && trim != "" {
                trim = "　　" + trim
            }
            return Substring(trim)
        })

        paragraph = paragraph.filter {$0 != ""}
        return paragraph.joined(separator: "\r\n\r\n")
    }
}
