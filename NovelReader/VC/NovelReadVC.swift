//
//  NovelReadVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 14/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit

class NovelReadVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var contentTable: UITableView!
    @IBOutlet var pageSlider: UISlider!
    @IBOutlet var velocityLabel: UILabel!
    @IBOutlet var pageLabel: UILabel!
    @IBOutlet var bottomViewBottom: NSLayoutConstraint!
    @IBOutlet var scrollSwitch: UISwitch!
    @IBOutlet var favoriteBtn: UIButton!
    
    private let cellID = "NovelTextTableCell"
    private var isFirst = true
    private var currentPage = 1
    private var lastPage = -1
    private var contents = [ContentModel]()
    private var timer: Timer?
    private var scrollToBottom = false
    private let config = DataBase.getConfig()
    var intro: NovelIntro?
    var favorite: FavoriteNovelModel?
    
    class func instance() -> NovelReadVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: VCNames.novelReadVC.rawValue) as! NovelReadVC
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.contentTable.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor(hex: self.config.bgColor)
        self.addObserver(names: [.NovelContent, .ModifyData])
        if self.isFirst {
            HTTPClient.getNovelContent(tid: (self.intro?.tid)!, page: self.currentPage)
            self.isFirst = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeNotiObserver()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickPageBtn(_ sender: UIButton) {
        (sender.tag == 1) ? self.toNextPage(): self.toLastPage()
    }
    
    @IBAction func clickVelocityBtn(_ sender: UIButton) {
        var v = Int(self.velocityLabel.text!)!
        v += sender.tag
        if v > 0 {
            self.velocityLabel.text = "\(v)"
        }
    }
    
    @IBAction func dragPageSlider(_ sender: UISlider) {
        let page = sender.value
        let str = "\(ceil(page))".substring(from: 0, to: -3)
        self.pageLabel.text = "\(str)/\(self.lastPage)"
    }
    
    @IBAction func endDragPageSlider(_ sender: UISlider) {
        let page = sender.value
        let target = Int(ceil(page))
        HTTPClient.getNovelContent(tid: (self.intro?.tid)!, page: target)
        self.view.makeToastActivity()
    }
    
    @IBAction func clickScrollSwitch(_ sender: UISwitch) {
        let isOn = sender.isOn
        if isOn {
            self.timer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(self.scrollTable), userInfo: nil, repeats: true)
        } else {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    @IBAction func clickDownloadBtn(_ sender: Any) {
                
    }
    
    @IBAction func clickBookMarkBtn(_ sender: UIButton) {
        if sender.isSelected {
            // want to cancel favorite
            DataBase.removeFavorite(tid: (self.intro?.tid)!)
            sender.isSelected = false
        } else {
            guard let info = self.intro else {return}
            let favorite = FavoriteNovelModel(banner: info.banner?.absoluteString, tid: info.tid, title: info.title)
            DataBase.addFavorite(info: favorite)
            sender.isSelected = true
        }
    }
    
}

// MARK: Network
extension NovelReadVC {
    override func update(noti: Notification) {
        DispatchQueue.main.async {
            let notiName = Notification.Name.self
            switch noti.name {
            case notiName.NovelContent:
                let res = noti.object as! NovelContent
                self.updateDataBy(res: res)
            case notiName.ModifyData:
                let (success, res) = noti.object as! (Bool, EditInfo)
                self.navigateToEdit(success: success, res: res)
            default: return
            }
        }
    }
    
    func updateDataBy(res: NovelContent) {
        self.view.hideToastActivity()

        self.currentPage = res.currentPage
        self.lastPage = res.lastPage
        self.contents = res.contents
        self.contentTable.reloadData()
        let row = self.favorite?.readRow ?? 0
        self.favorite = nil
        if self.scrollToBottom {
            self.scrollToBottom = false
            let idx = self.contents.count - 1
            self.contentTable.scrollToRow(at: IndexPath(row: idx, section: 0), at: .top, animated: false)
        } else {
            self.contentTable.scrollToRow(at: IndexPath(row: row, section: 0), at: .top, animated: false)
        }
        
        self.view.makeToast(message: "\(self.currentPage)/\(self.lastPage)")
        
        self.pageSlider.maximumValue = Float(res.lastPage)
        self.pageSlider.value = Float(res.currentPage)
        self.pageLabel.text = "\(res.currentPage)/\(res.lastPage)"
    }
    
    func navigateToEdit(success: Bool, res: EditInfo) {
        self.view.hideToastActivity()
        if !success {
            self.showNoticeAlert(title: nil, msg: res.message)
            return
        }
        
        self.setBackBtn()
        let vc = EditVC.instance()
        vc.modifyData = res
        self.navigationController?.show(vc, sender: self)
    }
}

// MARK: Tableview
extension NovelReadVC: UITableViewDelegate, UITableViewDataSource, NovelTextTableCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as! NovelTextTableCell
        let info = self.contents[indexPath.row]
        cell.backgroundColor = UIColor.clear
        cell.content.tag = indexPath.row
        cell.content.text = info.data
        let hex = self.config.frontColor
        cell.content.textColor = UIColor(hex: hex)
        cell.editBtn.isHidden = (info.url == nil) ? true: false
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.favoriteBtn.isSelected {
            DataBase.setBookMark(tid: (self.intro?.tid)!, readPage: self.currentPage, readRow: indexPath.row)
        }
    }
    
    func editContent(row: Int) {
        self.view.makeToastActivity()
        let urlStr = "https://ck101.com/" + self.contents[row].url!
        if let url = URL(string: urlStr) {
            HTTPClient.fetchModifyData(url: url, row: row)
        } else {
            self.navigateToEdit(success: false, res: EditInfo())
        }
    }
    
    func toPreviousChapter(row: Int) {
        if row > 0 {
            let index = IndexPath(row: row-1, section: 0)
            self.contentTable.scrollToRow(at: index, at: .top, animated: false)
            self.setBarVisibility(hidden: true)
        } else {
            self.scrollToBottom = true
            self.toLastPage()
        }
    }
    
    func toNextChapter(row: Int) {
        let last = self.contents.count
        if row < last-1 {
            let index = IndexPath(row: row+1, section: 0)
            self.contentTable.scrollToRow(at: index, at: .top, animated: false)
            self.setBarVisibility(hidden: true)
        } else if row == last-1 {
            self.toNextPage()
        }
    }
}

// MARK: Scroll
extension NovelReadVC: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let hidden = (velocity.y >= 0) ? true: false
        self.setBarVisibility(hidden: hidden)
    }
}

// MARK: Func
extension NovelReadVC {
    func viewInit() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = self.intro?.title
        
        let nib = UINib(nibName: self.cellID, bundle: nil)
        self.contentTable.register(nib, forCellReuseIdentifier: self.cellID)
        self.contentTable.tableFooterView = UIView(frame: CGRect.zero)
        self.contentTable.rowHeight = UITableViewAutomaticDimension
        self.contentTable.estimatedRowHeight = 2700

        self.setGesture()

        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "refresh"), style: .plain, target: self, action: #selector(self.reloadPage))
        self.navigationItem.rightBarButtonItem = item
        
        self.favorite = DataBase.isFavorite(tid: (self.intro?.tid)!)
        if self.favorite != nil {
            self.currentPage = (self.favorite?.readPage)!
            self.favoriteBtn.isSelected = true
        }
    }
    
    func setGesture() {
        if self.config.popByEdgePan {
            let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.toNextPage))
            leftSwipe.direction = .left
            leftSwipe.require(toFail: (self.navigationController?.interactivePopGestureRecognizer)!)
            self.contentTable.addGestureRecognizer(leftSwipe)

            let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.toLastPage))
            rightSwipe.direction = .right
            rightSwipe.require(toFail: (self.navigationController?.interactivePopGestureRecognizer)!)
            self.contentTable.addGestureRecognizer(rightSwipe)
        }
        
        let pinchGes = UIPinchGestureRecognizer(target: self, action: #selector(self.zoomTextSize(ges:)))
        self.contentTable.addGestureRecognizer(pinchGes)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapView))
        self.contentTable.addGestureRecognizer(tap)
    }
    
    @objc func tapView() {
        let status = self.navigationController?.isNavigationBarHidden
        self.setBarVisibility(hidden: !status!)
    }
    
    func setBarVisibility(hidden: Bool) {
        let offset: CGFloat = hidden ? -90: 0
        UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.navigationController?.setNavigationBarHidden(hidden, animated: true)
            self.bottomViewBottom.constant = offset
            self.view.updateConstraintsIfNeeded()
        }, completion: nil)
    }
    
    @objc func toLastPage() {
        let page = self.currentPage - 1
        if 0 < page && page <= self.lastPage {
            HTTPClient.getNovelContent(tid: (self.intro?.tid)!, page: page)
            self.view.makeToastActivity()
        } else {
            self.view.makeToast(message: "上面沒有了")
        }
    }
    
    @objc func toNextPage() {
        let page = self.currentPage + 1
        if 0 < page && page <= self.lastPage {
            HTTPClient.getNovelContent(tid: (self.intro?.tid)!, page: page)
            self.view.makeToastActivity()
        } else {
            self.view.makeToast(message: "下面沒有了")
        }
    }

    @objc func reloadPage() {
        HTTPClient.getNovelContent(tid: (self.intro?.tid)!, page: self.currentPage)
        self.view.makeToastActivity()
    }
    
    @objc func zoomTextSize(ges: UIPinchGestureRecognizer) {
        print("v: \(ges.velocity)")
        print("scale: \(ges.scale)")
        print(ges.state)
        // TODO text zoom
    }

    @objc func scrollTable() {
        let velocity = self.velocityLabel.text!.toCGFloat()!
        var offset = self.contentTable.contentOffset
        offset.y += velocity * 5.0
        
        let height = self.contentTable.bounds.size.height
        if offset.y > self.contentTable.contentSize.height - height {
            offset.y = self.contentTable.contentSize.height - height
            self.scrollSwitch.setOn(false, animated: false)
            self.clickScrollSwitch(self.scrollSwitch)
        }
        self.contentTable.setContentOffset(offset, animated: true)
    }

    func setContent(_ content: String, In row: Int) {
        self.contents[row].data = content
        self.contentTable.beginUpdates()
        let path = IndexPath(row: row, section: 0)
        let cell = self.contentTable.cellForRow(at: path) as! NovelTextTableCell
        cell.content.text = content
        self.contentTable.endUpdates()
    }
}
