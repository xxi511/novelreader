//
//  WordDicTableVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 28/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit

class WordDicTableVC: UITableViewController {

    private let config = DataBase.getConfig()
    var dics = [WordDicContent]()
    var searchRes = [WordDicContent]()
    var searchController = UISearchController(searchResultsController: nil)


    class func instance() -> WordDicTableVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: VCNames.wordDicTableVC.rawValue) as! WordDicTableVC
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchController.isActive = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: UITableview
extension WordDicTableVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchController.isActive ? self.searchRes.count: self.dics.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordTableCell", for: indexPath) as! WordTableCell
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        let words = self.searchController.isActive ? self.searchRes[indexPath.row]: self.dics[indexPath.row]
        cell.oldText.text = words.strOld
        cell.newText.text = words.strNew
        cell.arrow.textColor = UIColor(hex: self.config.frontColor)
        return cell
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return (tableView.isEditing) ? .insert: .delete
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        var idx: Int
        var word: WordDicContent

        if editingStyle == .delete {
            if self.searchController.isActive {
                word = self.searchRes.remove(at: indexPath.row)
                idx = self.dics.index(of: word)!
                self.dics.remove(at: idx)
            } else {
                word = self.dics.remove(at: indexPath.row)
                idx = indexPath.row
            }
            tableView.deleteRows(at: [indexPath], with: .automatic)
            DataBase.removeWordFromDic(index: idx)

        } else if editingStyle == .insert {
            if self.searchController.isActive {
                word = self.searchRes[indexPath.row]
                idx = self.dics.index(of: word)!
                self.searchController.searchBar.resignFirstResponder()
            } else {
                idx = indexPath.row
            }
            self.addWord(row: idx)
        }
    }
}

extension WordDicTableVC: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchController.isActive = true
        guard searchText != "" else {
            self.searchRes = self.dics
            self.tableView.reloadData()
            return
        }
        self.searchRes = self.dics.filter {$0.strOld.contains(searchText) || $0.strNew.contains(searchText)}
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.isActive = false
        self.tableView.reloadData()
    }
}

// MARK: Funcs
extension WordDicTableVC {
    func viewInit() {
        let nib = UINib(nibName: "WordTableCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "WordTableCell")
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.dics = DataBase.loadWordDic()
        self.tableView.reloadData()
        self.searchControllerInit()

        let item = UIBarButtonItem(title: "＋", style: .plain, target: self, action: #selector(self.clickAddBtn(btn:)))
        self.navigationItem.rightBarButtonItem = item

        self.tableView.backgroundColor = UIColor(hex: self.config.bgColor)
    }
    
    private func searchControllerInit() {
        self.searchController.searchResultsUpdater = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchBarInit(bar: self.searchController.searchBar)
    }
    
    private func searchBarInit(bar: UISearchBar) {
        bar.placeholder = "輸入文字進行搜尋"
        bar.setValue("取消", forKey: "cancelButtonText")
        let hexColor = DataBase.getConfig().bgColor
        bar.barTintColor = UIColor(hex: hexColor)
        bar.delegate = self
        bar.sizeToFit()
        self.navigationItem.titleView = bar
    }

    @objc private func clickAddBtn(btn: UIBarButtonItem) {
        guard self.dics.count > 0 else {
            self.addWord(row: -1)
            return
        }

        if self.tableView.isEditing {
            btn.title = "＋"
            self.tableView.isEditing = false
        } else {
            btn.title = "Done"
            self.tableView.isEditing = true
        }
    }
    
    private func addWord(row: Int) {
        let alert = UIAlertController(title: "請輸入文字", message: nil, preferredStyle: .alert)
        alert.addTextField { oldText in
            oldText.placeholder = "錯誤的文字"
        }
        alert.addTextField { newText in
            newText.placeholder = "正確的文字，清空填寫!@#$%"
        }

        let bottomTitle = (row == -1) ? "儲存": "存下面"
        let saveBottom = UIAlertAction(title: bottomTitle, style: .default) { _ in
            var old = ""
            var new = ""
            for textField in alert.textFields! {
                if textField.placeholder == "錯誤的文字" {
                    old = textField.text!
                } else {
                    new = textField.text!
                }
            }
            self.saveWord(old: old, new: new, row: row+1)
        }
        
        let saveTop = UIAlertAction(title: "存上面", style: .default) { _ in
            var old = ""
            var new = ""
            for textField in alert.textFields! {
                if textField.placeholder == "錯誤的文字" {
                    old = textField.text!
                } else {
                    new = textField.text!
                }
            }
            self.saveWord(old: old, new: new, row: row-1)
        }

        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        if row != -1 {
            alert.addAction(saveTop)
        }
        alert.addAction(saveBottom)
        alert.addAction(cancel)
        if self.searchController.isActive {
            self.searchController.present(alert, animated: true, completion: nil)
        } else {
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func saveWord(old: String, new: String, row: Int) {
        guard old != "" && new != "" else {
            self.showNoticeAlert(title: nil, msg: "請填寫完整資料")
            return
        }
        let idx = max(row, 0)
        let word = WordDicContent.initWith(old, new: new)
        self.dics.insert(word, at: idx)
        DataBase.saveWordToDic(word: word, at: idx)

        if self.searchController.isActive {
            self.searchBar(self.searchController.searchBar,
                           textDidChange: self.searchController.searchBar.text ?? "")
        } else {
            self.tableView.beginUpdates()
            let path = IndexPath(row: idx, section: 0)
            self.tableView.insertRows(at: [path], with: .fade)
            self.tableView.endUpdates()
        }

    }
}
