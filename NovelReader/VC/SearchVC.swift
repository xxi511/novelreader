//
//  SearchVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 11/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    @IBOutlet private var table: UITableView!
    @IBOutlet private var keywodField: UITextField!
    @IBOutlet private var cancelBtn: UIButton!
    private let config = DataBase.getConfig()
    var fid: String = ""
    var books = [SearchRes.Result.Book]()
    
    class func instance() -> SearchVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: VCNames.searchVC.rawValue) as! SearchVC
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver(names: [.SearchResult])
        if fid == "" {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeNotiObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickCancelBtn(_ sender: Any) {
        if self.keywodField.text == "" {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.keywodField.text = ""
            self.books.removeAll()
            self.table.reloadData()
        }
    }
    
}

// MARK: Network
extension SearchVC {
    override func update(noti: Notification) {
        if noti.name == Notification.Name.SearchResult {
            DispatchQueue.main.async {
                self.updateTable(searchRes: noti.object as! SearchRes)
            }
        }
    }
    
    func search(keyword: String) {
        HTTPClient.searchNovel(fid: self.fid, keyword: keyword)
    }
    
    func updateTable(searchRes: SearchRes) {
        guard searchRes.status == 0 else {
            let msg = searchRes.message
            self.showNoticeAlert(title: "發生錯誤", msg: msg)
            return
        }
        
        self.books = (searchRes.result?.arrayList)!
        self.table.reloadData()
    }
}

extension SearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let keyword = textField.text, keyword != "" else {return true}
        self.search(keyword: keyword)
        textField.resignFirstResponder()
        return true
    }
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        cell?.backgroundColor = UIColor.clear
        cell?.textLabel?.textColor = UIColor(hex: self.config.frontColor)
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.text = self.books[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NovelReadVC.instance()
        let intro = NovelIntro(banner: nil, time: "", page: -1,
                               tid: self.books[indexPath.row].book,
                               title: self.books[indexPath.row].name)
        vc.intro = intro
        self.navigationController?.show(vc, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: Funcs
extension SearchVC {
    func viewInit() {
        self.tableInit()
        self.title = "搜尋"
        let imgView = UIImageView(image: #imageLiteral(resourceName: "search")).setColor(hex: "989898")
        imgView.frame = CGRect(x: 0, y: 0, width: 10+20, height: 10)
        imgView.contentMode = .center
        self.keywodField.leftView = imgView
        self.keywodField.leftViewMode = .always
        self.table.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor(hex: self.config.bgColor)
        self.cancelBtn.tintColor = UIColor(hex: self.config.frontColor)
    }
    
    func tableInit() {
        self.table.tableFooterView = UIView(frame: CGRect.zero)
        self.table.rowHeight = UITableViewAutomaticDimension
        self.table.estimatedRowHeight = 80
    }
}
