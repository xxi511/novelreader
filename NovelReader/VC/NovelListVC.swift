//
//  NovelListVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 13/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit
import ESPullToRefresh
import Kingfisher

class NovelListVC: UIViewController {
    var type: HTTPClient.NovelType = .long
    var currentPage = 1
    var lastPage = -1
    var isFirst = true
    var novelList = [NovelIntro]()
    private let config = DataBase.getConfig()
    
    @IBOutlet var novelTable: UITableView!
    
    class func instance() -> NovelListVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: VCNames.novelListVC.rawValue) as! NovelListVC
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.viewInit()
        // TODO title
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.novelTable.backgroundColor = UIColor(hex: self.config.bgColor)
        self.addObserver(names: [.NovelList])
        if self.isFirst {
            HTTPClient.getNovelList(type: self.type, page: 1)
            self.isFirst = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeNotiObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: Network
extension NovelListVC {
    override func update(noti: Notification) {
        DispatchQueue.main.async {
            let notiName = Notification.Name.self
            switch noti.name {
            case notiName.NovelList:
                let res = noti.object as! NovelList
                self.updateDataBy(res: res)
            default: return
            }
        }
    }
    
    func updateDataBy(res: NovelList) {
        let resTitle = HtmlParse.zoneArr[res.zone]
        guard resTitle == self.title! else {return} // make sure res is what i want

        self.currentPage = res.currentPage
        self.lastPage = res.lastPage
        if res.currentPage == 1 {
            self.novelList = res.novels
            self.novelTable.es.stopPullToRefresh()
        } else {
            self.novelList += res.novels
            self.novelTable.es.stopLoadingMore()
        }
        self.novelTable.reloadData()
    }
}

// MARK: Table View
extension NovelListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.novelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NovelIntroTableCell", for: indexPath) as! NovelIntroTableCell
        cell.backgroundColor = UIColor.clear
        let intro = self.novelList[indexPath.row]
        cell.title.text = intro.title
        cell.title.textColor = UIColor(hex: self.config.frontColor)
        cell.banner.kf.setImage(with: intro.banner, placeholder: #imageLiteral(resourceName: "noPhoto"))
        cell.updateTime.text = intro.time
        cell.updateTime.textColor = UIColor(hex: self.config.frontColor)
        cell.totalPage.text = (intro.page == 0) ? "": "共\(intro.page)頁"
        cell.totalPage.textColor = UIColor(hex: self.config.frontColor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NovelReadVC.instance()
        vc.intro = self.novelList[indexPath.row]
        self.navigationController?.show(vc, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: Funcs
extension NovelListVC {
    func viewInit() {
        let nib = UINib(nibName: "NovelIntroTableCell", bundle: nil)
        self.novelTable.register(nib, forCellReuseIdentifier: "NovelIntroTableCell")
        self.novelTable.tableFooterView = UIView(frame: CGRect.zero)
        self.novelTable.rowHeight = UITableViewAutomaticDimension
        self.novelTable.estimatedRowHeight = 80
        
        self.novelTable.es.addPullToRefresh {
            HTTPClient.getNovelList(type: self.type, page: 1)
        }
        
        self.novelTable.es.addInfiniteScrolling {
            if self.currentPage + 1 <= self.lastPage {
                HTTPClient.getNovelList(type: self.type, page: self.currentPage + 1)
            } else {
                self.novelTable.es.noticeNoMoreData()
            }
        }
        
        if self.type != .favorite {
            let item = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(self.clickSearch))
            self.navigationItem.rightBarButtonItem = item
        }
    }

    @objc func clickSearch() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
        let vc = SearchVC.instance()
        let fid = self.type.rawValue.split(separator: "-")[1]
        vc.fid = String(fid)
        self.navigationController?.show(vc, sender: self)
    }
}
