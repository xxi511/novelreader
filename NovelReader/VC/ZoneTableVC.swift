//
//  ZoneTableVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 12/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit
import SafariServices

class ZoneTableVC: UITableViewController {

    private var numArray = [HomeList]()
    private var isConfigDone = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver(names: [.NovelList, .HomeList])
        self.getHomePage()
        self.setColorByConfig()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let num = DataBase.getFavorites().count
        if  num > 0 {
            guard let label = self.view.viewWithTag(9) as? UILabel else {return}
            label.text = "書籤列表(\(num))"

        }
        if !self.isConfigDone {
            self.setLabelColorByConfig()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeNotiObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: Network
extension ZoneTableVC {
    func getHomePage() {
        self.numArray.removeAll()
        HTTPClient.getHomeList()
        HTTPClient.getNovelList(type: .end)
        HTTPClient.getNovelList(type: .originalLoveEnd)
    }
    
    override func update(noti: Notification) {
        DispatchQueue.main.async {
            switch noti.name.rawValue {
            case "HomeList":
                let homeList = noti.object as! [HomeList]
                self.updateInfoByHomeList(res: homeList)
            case "NovelList":
                let novelList = noti.object as! NovelList
                self.updateInfoByNovelList(res: novelList)
            default: return
            }
        }
    }
    
    func updateInfoByHomeList(res: [HomeList]) {
        self.numArray += res
        self.correctContent()
    }
    
    func updateInfoByNovelList(res: NovelList) {
        guard res.zone != -1 else {return}
        let info = HomeList(zone: res.zone, today: res.today, subject: res.subject)
        self.numArray.append(info)
        self.correctContent()
    }
}

// MARK: - Table view
extension ZoneTableVC {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let zone = indexPath.row + 1
        let label = self.view.viewWithTag(zone) as! UILabel
        let title = label.text!.substring(from: 0, to: 3)

        let vc = NovelListVC.instance()
        vc.type = HTTPClient.idx2NovelType(idx: zone)
        vc.title = title
        self.navigationController?.show(vc, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: Funcs
extension ZoneTableVC {
    private func viewInit() {
        self.navigationController?.navigationBar.topItem?.title = "看小說"
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "setting"), style: .plain, target: self, action: #selector(self.clickSetting))
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = item
    }

    private func setColorByConfig() {
        let config = DataBase.getConfig()
        self.setBarColor(hex: config.bgColor)
        self.setBarItemColor(hex: config.frontColor)
        self.tableView.backgroundColor = UIColor(hex: config.bgColor)
        self.setLabelColorByConfig()
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = UIColor(hex: config.bgColor)
    }

    private func setLabelColorByConfig() {
        let config = DataBase.getConfig()
        for i in 0...9 {
            if let label = self.view.viewWithTag(i) as? UILabel {
                label.textColor = UIColor(hex: config.frontColor)
            } else {
                self.isConfigDone = false
                return
            }
        }
    }

    @objc private func clickSetting() {
        let vc = SettingTableVC.instance()
        self.navigationController?.show(vc, sender: self)
    }

    private func correctContent() {
        guard self.numArray.count == 8 else {return}
        let longN = self.numArray.index(where: {$0.zone == 1})!
        let doneN = self.numArray.index(where: {$0.zone == 2})!
        self.numArray[longN].subject -= self.numArray[doneN].subject
        for info in self.numArray {
            if let label = self.view.viewWithTag(info.zone) as? UILabel {
                let title = label.text!.substring(from: 0, to: 3)
                label.text = "\(title)\n主題：\(info.subject)"
            }
        }
    }
}
