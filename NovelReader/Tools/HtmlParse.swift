//
//  HtmlParse.swift
//  NovelReader
//
//  Created by 陳建佑 on 07/09/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import Kanna


class HtmlParse {
    
    static let zoneArr = ["首頁", "長篇小說", "全篇小說", "原創言情",
                          "原創全本", "出版言情", "耽美小說", "日系小說",
                          "短篇小說", "書籤列表"]
    
    class func parseNovelList(data: Data) -> NovelList {
        var res = NovelList()
        do {
            let doc = try HTML(html: data, encoding: .utf8)
            
            res.currentPage = Int((doc.at_css(".mn #pgt #fd_page_top .pg strong")?.content!)!)!
            if doc.at_css("#fd_page_top .pg .nxt") != nil {
                let last = doc.css(".mn #pgt #fd_page_top .pg a").reversed()[1].content
                let lastStr = (last?.contains("..."))! ? last?.substring(from: 4, to: -1): last
                res.lastPage = Int(lastStr!)!
            } else {
                res.lastPage = res.currentPage
            }
            
            let ruleBox = doc.at_css(".ruleBox")!
            res.zone = zoneArr.index(of: ruleBox.at_css(".xs2 a")!["title"]!) ?? -1
            for (idx, info) in ruleBox.css(".xs1 .xi1").enumerated() {
                if idx == 0 {
                    res.today = Int(info.content!) ?? 0
                } else if idx == 1 {
                    res.subject = Int(info.content!) ?? 0
                }
            }
            
            for node in doc.css("#threadlist form tbody") {
                if !(node["id"]?.contains("normalthread"))! {
                    // ID為normal開頭的才是小說，其他都不要
                    continue
                }
                
                let tid = node["tid"]!
                let imgNode = node.at_css("img")!
                let title = imgNode["alt"]
                let img = imgNode["src"]
                let time = node.at_css(".lastpost_time")?.content!
                
                var lastPage = 1
                if node.innerHTML!.contains("class=\"tps\"") {
                    let pageSpan = node.css(".tps a")
                    lastPage = Int(pageSpan[pageSpan.count-1].content!)!
                }
                
                let intro = NovelIntro(banner: URL(string: img!), time: time!,
                                       page: lastPage, tid: tid, title: title!)
                res.novels.append(intro)
            }
            return res
        } catch {return res}
    }
    
    class func parseHomeList(data: Data) -> [HomeList] {
        var resList: [HomeList] = []
        do {
            let doc = try HTML(html: data, encoding: .utf8)
            for node in doc.css(".fl_g .fl_dl") {
                let title = node.at_css("h2 a")!.content!
                
                let todayStr = node.at_css("h2 em")?.content!.substring(from: 2, to: -2)
                let today = (todayStr != nil) ? Int(todayStr!): 0
                
                let subjectStr = node.at_css("dl dd em")?.content!.substring(from: 4, to: -1)
                let subjectTitleStr = node.at_css("dl dd em span")?["title"]
                let subject = (Int(subjectStr!) != nil) ? Int(subjectStr!): Int(subjectTitleStr!)
                
                if let index = zoneArr.index(of: title) {
                    let list = HomeList(zone: index, today: today!, subject: subject!)
                    resList.append(list)
                }
            }
            return resList
        } catch {return resList}
    }

    class func parseNovelContent(data: Data) -> NovelContent {
        var res = NovelContent()
        do {
            let doc = try HTML(html: data, encoding: .utf8)
            if doc.at_css(".pgt .pg strong") != nil {
                res.currentPage = Int((doc.at_css(".pgt .pg strong")?.content!)!)!
            } else {
                res.currentPage = 1
            }
            
            if doc.at_css(".pgt .pg .nxt") != nil {
                let last = doc.css(".pgt .pg a").reversed()[1].content
                let lastStr = (last?.contains("..."))! ? last?.substring(from: 4, to: -1): last
                res.lastPage = Int(lastStr!)!
            } else {
                res.lastPage = res.currentPage
            }
            
            for article in doc.css(".plhin") {
                let edit = article.at_css(".editp")?["href"]
                var content = article.at_css(".t_f")?.content
                content = content?.trimmingCharacters(in: .whitespacesAndNewlines)
                content = content!.replace("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n", to: "")
                let editor = article.at_css(".t_f .pstatus")
                if editor != nil {
                    let info = (editor?.content)!.substring(from: 1, to: -2)
                    content = content?.replacingOccurrences(of: info, with: "")
                }
                let floor = Int((article.at_css(".postNum a em")?.content)!)
                let info = ContentModel(data: content!, url: edit, floor: floor!)
                res.contents.append(info)
            }
            return res
        } catch {return res}
    }
    
    class func parseModifyData(data: Data) -> (Bool, EditInfo) {
        var info = EditInfo()
        
        do {
            let doc = try HTML(html: data, encoding: .utf8)
            guard let text = doc.at_css(".area #e_textarea") else {
                if let err = doc.at_css(".alert_error p")?.content {
                    info.message = err
                }
                return (false, info)
            }

            info.message = text.content!
            info.formhash = doc.at_css("input[name=formhash]")!["value"]!
            info.posttime = doc.at_css("input[name=posttime]")!["value"]!
            info.delattachop = doc.at_css("input[name=delattachop]")!["value"]!
            info.wysiwyg = doc.at_css("input[name=wysiwyg]")!["value"]!
            info.fid = doc.at_css("input[name=fid]")!["value"]!
            info.tid = doc.at_css("input[name=tid]")!["value"]!
            info.pid = doc.at_css("input[name=pid]")!["value"]!
            info.page = doc.at_css("input[name=page]")!["value"]!
            info.subject = doc.at_css("input[name=subject]")!["value"]!
            if let select = doc.at_css("select[name=typeid]") {
                for option in select.css("option") where option["selected"] != nil {
                    info.typeid = option["value"]!
                    break
                }
            }
            
            return (true, info)
        } catch {return (false, info)}
    }
    
    class func parseSendModifyResult(data: Data) -> Bool {
        let txt = String(data: data, encoding: .utf8)
        return (txt?.contains("帖子編輯成功"))! ? true: false
    }
}
