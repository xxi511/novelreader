//
//  Models.swift
//  NovelReader
//
//  Created by 陳建佑 on 21/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
struct HomeList {
    var zone = -1
    var today = 0
    var subject = 0
}

struct NovelIntro {
    var banner: URL?
    var time = "" // last update time
    var page = -1 // total page
    var tid = ""
    var title = ""
}

struct NovelList {
    var currentPage = -1
    var lastPage = -1
    var zone = -1
    var today = 0
    var subject = 0
    var novels = [NovelIntro]()
}

struct NovelContent {
    var currentPage = -1
    var lastPage = -1
    var contents = [ContentModel]()
}

struct SearchRes: Codable {
    var status: Int
    var message: String
    
    struct Result: Codable {
        struct Book: Codable {
            var book: String
            var name: String
        }
        var arrayList: [Book]
    }
    
    var result: Result?
}

struct EditInfo: Codable {
    var formhash: String = ""
    var posttime: String  = ""
    var delattachop: String  = ""
    var wysiwyg: String  = ""
    var fid: String  = ""
    var tid: String  = ""
    var pid: String = ""
    var page: String = ""
    var subject: String = ""
    var typeid: String?
    var message: String = "發生錯誤，請稍後再試"
    var checkbox = "0"
    var usesig = "1"
    var delete = "0"
    var save = ""
    var uploadalbum = "-2"
    var newalbum = "請輸入相冊名稱"
    var row = -1
}
