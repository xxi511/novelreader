//
//  DataBase.swift
//  NovelReader
//
//  Created by 陳建佑 on 25/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import RealmSwift

class DataBase {
    class func deleteDB() {
        do {
            try FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
        } catch {}
    }
}

// MARK: Word Dictionary
extension DataBase {
    private class func loadBaseWordDic() -> [WordDicContent] {
        guard let path = Bundle.main.path(forResource: "BaseWordDic", ofType: nil) else {return []}
        do {
            let content = try String(contentsOfFile: path, encoding: .utf8)
            let dics = content.split(separator: "\n").map({ str -> WordDicContent in
                let sep = str.split(separator: " ")
                return WordDicContent.initWith(String(sep[0]), new: String(sep[1]))
            })
            
            let realm = try! Realm()
            let itemList = WordDicModel()
            try! realm.write {
                itemList.items.append(objectsIn: dics)
                realm.add(itemList)
            }
            return dics
        } catch {return []}
    }
    
    class func loadWordDic() -> [WordDicContent] {
        let realm = try! Realm()
        guard let wordDic = realm.objects(WordDicModel.self).first else {
            return DataBase.loadBaseWordDic()
        }
        return Array(wordDic.items)
    }
    
    class func saveWordToDic(word: WordDicContent, at: Int) {
        let realm = try! Realm()
        let itemList = realm.objects(WordDicModel.self).first!.items
        let pred = NSPredicate(format: "self.strOld == %@ AND self.strNew == %@ ", word.strOld, word.strNew)
        let allDic = itemList.filter(pred)
        guard allDic.count == 0 else {return}
        
        try! realm.write {
            itemList.insert(word, at: at)
        }
    }
    
    class func removeWordFromDic(index: Int) {
        let realm = try! Realm()
        let itemList = realm.objects(WordDicModel.self).first!.items
//        let pred = NSPredicate(format: "self.strOld == %@ AND self.strNew == %@ ", word.strOld, word.strNew)
//        guard let idx = itemList.index(matching: pred) else {return}

        try! realm.write {
            itemList.remove(at: index)
        }
    }
}

// MARK: Favorite
extension DataBase {
    class func getFavorites() -> [NovelIntro] {
        let realm = try! Realm()
        let res = realm.objects(FavoriteNovelModel.self)
        var arr = [NovelIntro]()
        for item in res {
            let url = URL(string: item.banner ?? "")
            let intro = NovelIntro(banner: url, time: "", page: 0, tid: item.tid, title: item.title)
            arr.append(intro)
        }
        return arr
    }
    
    class func isFavorite(tid: String) -> FavoriteNovelModel? {
        let realm = try! Realm()
        let pred = NSPredicate(format: "self.tid == %@", tid)
        let res = realm.objects(FavoriteNovelModel.self).filter(pred).first
        guard let item = res else {
            return nil
        }
        return item
    }
    
    class func addFavorite(info: FavoriteNovelModel) {
        let realm = try! Realm()
        let pred = NSPredicate(format: "self.tid == %@", info.tid)
        let res = realm.objects(FavoriteNovelModel.self).filter(pred)
        try! realm.write {
            realm.delete(res)
            realm.add(info)
        }
    }
    
    class func setBookMark(tid: String, readPage: Int, readRow: Int) {
        let realm = try! Realm()
        let pred = NSPredicate(format: "self.tid == %@", tid)
        let res = realm.objects(FavoriteNovelModel.self).filter(pred).first
        guard let item = res else {return}
        try! realm.write {
            item.readPage = readPage
            item.readRow = readRow
        }
    }
    
    class func removeFavorite(tid: String) {
        let realm = try! Realm()
        let pred = NSPredicate(format: "self.tid == %@", tid)
        let res = realm.objects(FavoriteNovelModel.self).filter(pred)
        try! realm.write {
            realm.delete(res)
        }
    }
}

// MARK: APP Configer
extension DataBase {
    class func initDB() {
        let realm = try! Realm()
        if realm.objects(ConfigersModel.self).first != nil {return}
        let config = ConfigersModel()
        try! realm.write {
            realm.add(config)
        }
    }

    class func getConfig() -> ConfigersModel {
        let realm = try! Realm()
        return realm.objects(ConfigersModel.self).first!
    }

    class func setConfig(config: ConfigersModel) {
        let realm = try! Realm()
        let oldConfig = realm.objects(ConfigersModel.self)
        try! realm.write {
            realm.delete(oldConfig)
            realm.add(config)
        }
    }

    class func setBackgroundColor(_ color: String) {
        let realm = try! Realm()
        let config = realm.objects(ConfigersModel.self).first
        try! realm.write {
            config?.bgColor = color
        }
    }

    class func setFrontColor(_ color: String) {
        let realm = try! Realm()
        let config = realm.objects(ConfigersModel.self).first
        try! realm.write {
            config?.frontColor = color
        }
    }

    class func setAccount(_ account: String?, pass: String?) {
        let realm = try! Realm()
        let config = realm.objects(ConfigersModel.self).first
        try! realm.write {
            config?.account = account
            config?.password = pass
        }
    }

    class func setPopOption(_ option: Bool) {
        let realm = try! Realm()
        let config = realm.objects(ConfigersModel.self).first
        try! realm.write {
            config?.popByEdgePan = option
        }
    }
}
