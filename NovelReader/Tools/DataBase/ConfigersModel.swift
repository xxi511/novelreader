//
//  Configers.swift
//  NovelReader
//
//  Created by 陳建佑 on 29/05/2018.
//  Copyright © 2018 陳建佑. All rights reserved.
//

import Foundation
import RealmSwift

class ConfigersModel: Object {
    @objc dynamic var bgColor = "FFFFFF"
    @objc dynamic var frontColor = "000000"
    @objc dynamic var account: String?
    @objc dynamic var password: String?
    @objc dynamic var popByEdgePan = true
}
