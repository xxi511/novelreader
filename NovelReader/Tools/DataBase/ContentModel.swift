//
//  ContentModel.swift
//  NovelReader
//
//  Created by 陳建佑 on 26/06/2018.
//  Copyright © 2018 陳建佑. All rights reserved.
//

import Foundation
import RealmSwift

class ContentModel: Object {
    @objc dynamic var data: String = ""
    @objc dynamic var url: String?
    @objc dynamic var floor: Int = -1

    convenience init(data: String, url: String?, floor: Int) {
        self.init()
        self.data = data
        self.url = url
        self.floor = floor
    }
}
