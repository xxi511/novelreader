//
//  WordDicModel.swift
//  NovelReader
//
//  Created by 陳建佑 on 26/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//
import RealmSwift
import Foundation

class WordDicModel: Object {
    let items = List<WordDicContent>()
}

class WordDicContent: Object {
    @objc dynamic var strNew: String = ""
    @objc dynamic var strOld: String = ""
    
    class func initWith(_ old: String, new: String) -> WordDicContent {
        let obj = WordDicContent()
        obj.strOld = old
        obj.strNew = new
        return obj
    }
}
