//
//  FavoriteNovelModel.swift
//  NovelReader
//
//  Created by 陳建佑 on 29/12/2017.
//Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import RealmSwift

class FavoriteNovelModel: Object {
    @objc dynamic var banner: String?
    @objc dynamic var tid = ""
    @objc dynamic var title = ""
    @objc dynamic var readPage = 1
    @objc dynamic var readRow = 0
    
    convenience init(banner: String?, tid: String, title: String) {
        self.init()
        self.banner = banner
        self.tid = tid
        self.title = title
    }
}
