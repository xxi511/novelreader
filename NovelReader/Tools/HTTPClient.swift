//
//  HTTPClient.swift
//  NovelReader
//
//  Created by 陳建佑 on 07/09/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
class HTTPClient {
    static let notiName = Notification.Name.self
    static let baseUrl = "https://ck101.com/"
    
    enum NovelType: String {
        case home = "forum.php?gid=1180"            // 首頁
        case long = "forum-237-"            // 長篇小說
        case end = "forum-3419-"             // 長篇小說完
        case originalLove = "forum-3451-"    // 原創言情
        case originalLoveEnd = "forum-3474-" // 原創言情完
        case publishLove = "forum-1308-"     // 出版言情
        case bl = "forum-3446-"             // 耽美
        case japan = "forum-1288-"           // 日系
        case short = "forum-855-"           // 短篇
        case favorite = "Favorite"
        case discuss = "forum-856-"         // 討論
    }
    
    class func idx2NovelType(idx: Int) -> NovelType {
        let arr: [NovelType] = [.home, .long, .end, .originalLove, .originalLoveEnd, .publishLove, .bl, .japan, .short, .favorite, .discuss]
        return arr[idx]
    }
    
    class func getNovelList(type: NovelType, page: Int=1) {
        if type == .favorite {
            let res = NovelList(currentPage: 1, lastPage: 1, zone: 9, today: 0, subject: 0, novels: DataBase.getFavorites())
            NotificationCenter.default.post(name: notiName.NovelList, object: res)
            return
        }
        
        let tmp = type.rawValue + "\(page).html"
        let url = URL(string: baseUrl + tmp)!
        
        let sess = URLSession.shared
        sess.dataTask(with: url) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    let res = NovelList()
                    NotificationCenter.default.post(name: notiName.NovelList, object: res)
                    return
            }
            
            let res = HtmlParse.parseNovelList(data: data!)
            NotificationCenter.default.post(name: notiName.NovelList, object: res)
            }.resume()
    }
    
    class func getHomeList() {
        let url = URL(string: baseUrl + NovelType.home.rawValue)!
        let sess = URLSession.shared
        sess.dataTask(with: url) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    let res: [HomeList] = []
                    NotificationCenter.default.post(name: notiName.HomeList, object: res)
                    return
            }
            
            let res = HtmlParse.parseHomeList(data: data!)
            NotificationCenter.default.post(name: notiName.HomeList, object: res)
            }.resume()
    }
    
    class func getNovelContent(tid: String, page: Int) {
        let url = URL(string: baseUrl + "/thread-\(tid)-\(page)-1.html")!
        let sess = URLSession.shared
        sess.dataTask(with: url) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    let res = NovelContent()
                    NotificationCenter.default.post(name: notiName.NovelContent, object: res)
                    return
            }
            
            let res = HtmlParse.parseNovelContent(data: data!)
            NotificationCenter.default.post(name: notiName.NovelContent, object: res)
            }.resume()
    }
    
    class func login(account: String, password: String, completionHandler: @escaping (Bool, String) -> Swift.Void) {
        
        let url = URL(string: baseUrl + "/member.php?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes&inajax=1")
        var req = URLRequest(url: url!)
        req.httpMethod = "POST"
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let body = "username=\(account)&password=\(password.toMD5())&quickforward=yes&handlekey=ls"
        req.httpBody = body.data(using: .ascii)
        
        let sess = URLSession.shared
        sess.dataTask(with: req) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    completionHandler(false, "發生錯誤，請稍後再試")
                    return
            }
            
            let resStr = String(data: data!, encoding: .utf8)!
            if resStr.contains("歡迎您回來") {
                guard let s = resStr.index(of: "歡"), let e = resStr.index(of: "面") else {
                    completionHandler(true, "登入成功")
                    return
                }
                completionHandler(true, String(resStr[s...e]))
            } else {
                guard let s = resStr.index(of: "登"), let e = resStr.index(of: "次") else {
                    completionHandler(false, "登入失敗，請確認帳號密碼")
                    return
                }
                completionHandler(false, String(resStr[s...e]))
            }
            }.resume()
    }
    
    class func searchNovel(fid: String, keyword: String) {
        let url = URL(string: "http://www.cd-cat.com/book/search/search.php")!
        var req = URLRequest(url: url)
        req.httpMethod = "POST"
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let body = "fid=\(fid)&keyword=\(keyword)"
        req.httpBody = body.data(using: .utf8)
        
        let sess = URLSession.shared
        sess.dataTask(with: req) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    let res = SearchRes(status: 3, message: "發生錯誤", result: nil)
                    NotificationCenter.default.post(name: notiName.SearchResult, object: res)
                    return
            }
            
            do {
                let res = try JSONDecoder().decode(SearchRes.self, from: data!)
                NotificationCenter.default.post(name: notiName.SearchResult, object: res)
            } catch {
                let res = SearchRes(status: 3, message: error.localizedDescription, result: nil)
                NotificationCenter.default.post(name: notiName.SearchResult, object: res)
            }
        }.resume()
    }
    
    class func fetchModifyData(url: URL, row: Int) {
        let sess = URLSession.shared
        sess.dataTask(with: url) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    let res = EditInfo()
                    NotificationCenter.default.post(name: notiName.ModifyData, object: (false, res))
                    return
            }
            
            var (success, res) = HtmlParse.parseModifyData(data: data!)
            res.row = row
            NotificationCenter.default.post(name: notiName.ModifyData, object: (success, res))
        }.resume()
    }
    
    class func sendModifyData(info: EditInfo) {
        let url = URL(string: baseUrl + "forum.php?mod=post&action=edit&extra=&editsubmit=yes")
        var req = URLRequest(url: url!)
        req.httpMethod = "POST"
        req.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        req.setValue("Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11", forHTTPHeaderField: "User-Agent")
        var body = ""
        guard let dic = info.asDictionary() else {return}
        for (k, v) in dic {
            if k == "row" {
                continue
            }
            body += "\(k)=\(v)&"
        }
        body = body.substring(from: 0, to: -2)
        req.httpBody = body.data(using: .utf8)
        
        let sess = URLSession.shared
        sess.dataTask(with: req) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                    let errMsg = error?.localizedDescription ?? "發生錯誤"
                    let res: [String: Any] = ["success": false, "errMsg": errMsg]
                    NotificationCenter.default.post(name: notiName.SendModify, object: res)
                    return
            }
            
            let success = HtmlParse.parseSendModifyResult(data: data!)
            let errMsg = success ? "": "發生錯誤"
            let res: [String: Any] = ["success": success, "errMsg": errMsg]
            NotificationCenter.default.post(name: notiName.SendModify, object: res)
        }.resume()
    }
}
