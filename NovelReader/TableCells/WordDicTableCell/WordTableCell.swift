//
//  WordTableCell.swift
//  NovelReader
//
//  Created by 陳建佑 on 28/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit

class WordTableCell: UITableViewCell {
    @IBOutlet var oldText: UITextField!
    @IBOutlet var newText: UITextField!
    @IBOutlet var arrow: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
