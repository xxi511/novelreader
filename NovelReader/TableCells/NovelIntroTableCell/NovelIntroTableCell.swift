//
//  NovelIntroTableCell.swift
//  NovelReader
//
//  Created by 陳建佑 on 14/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit

class NovelIntroTableCell: UITableViewCell {

    @IBOutlet var banner: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var updateTime: UILabel!
    @IBOutlet var totalPage: UILabel!
    @IBOutlet var titleLeft: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
