//
//  NovelTextTableCell.swift
//  NovelReader
//
//  Created by 陳建佑 on 21/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import UIKit
protocol NovelTextTableCellDelegate: NSObjectProtocol {
    func editContent(row: Int)
    func toPreviousChapter(row: Int)
    func toNextChapter(row: Int)
}

class NovelTextTableCell: UITableViewCell {

    @IBOutlet var previousBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var editBtn: UIButton!
//    @IBOutlet var content: UITextView!
    
    @IBOutlet var content: UILabel!
    weak var delegate: NovelTextTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nextBtn.setCorner(5)
        self.editBtn.setCorner(5)
        self.previousBtn.setCorner(5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func clickEditBtn(_ sender: Any) {
        self.delegate?.editContent(row: self.content.tag)
    }
    
    @IBAction func clickNextBtn(_ sender: Any) {
        self.delegate?.toNextChapter(row: self.content.tag)
    }
    
    @IBAction func clickPreviousBtn(_ sender: Any) {
        self.delegate?.toPreviousChapter(row: self.content.tag)
    }
}
