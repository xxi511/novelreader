//
//  ExForVC.swift
//  NovelReader
//
//  Created by 陳建佑 on 07/09/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
    
    enum VCNames: String {
        case novelListVC = "NovelListVC"
        case novelReadVC = "NovelReadVC"
        case settingTableVC = "SettingTableVC"
        case searchVC = "SearchVC"
        case editVC = "EditVC"
        case wordDicTableVC = "WordDicTableVC"
    }
    
    @objc func update(noti: Notification) {
        assert(false, "Not Implement update function")
    }
    
    func addObserver(names: [Notification.Name]) {
        for name in names {
            NotificationCenter.default.addObserver(self, selector: #selector(update(noti:)), name: NSNotification.Name(rawValue: name.rawValue), object: nil)
        }
    }
    
    func removeNotiObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func showNoticeAlert(title: String?, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let done = UIAlertAction(title: "知道了", style: .default, handler: nil)
        alert.addAction(done)
        if self.navigationController == nil {
            self.present(alert, animated: true, completion: nil)
        } else {
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func setBackBtn() {
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = item
    }
    
    func lastVC() -> UIViewController? {
        guard let vcs = self.navigationController?.viewControllers else {
            return nil
        }
        
        let idx = vcs.count - 2
        return (idx > 0) ? vcs[idx]: nil
    }

    func setBarColor(hex: String) {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: hex)
    }

    func setBarColor(color: UIColor) {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = color
    }

    func setBarItemColor(hex: String) {
        self.navigationController?.navigationBar.tintColor = UIColor(hex: hex)
        let textAttributes = [NSAttributedStringKey.foregroundColor: UIColor(hex: hex)]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    func setBarItemColor(color: UIColor) {
        self.navigationController?.navigationBar.tintColor = color
        let textAttributes = [NSAttributedStringKey.foregroundColor: color]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}
