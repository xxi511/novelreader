//
//  ImageViewEX.swift
//  NovelReader
//
//  Created by 陳建佑 on 12/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView {
    func setColor(hex: String) -> UIImageView {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = UIColor(hex: hex)
        return self
    }
}
