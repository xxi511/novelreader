//
//  CodableEX.swift
//  NovelReader
//
//  Created by 陳建佑 on 26/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
extension Encodable {
    func asDictionary() -> [String: Any]? {
        do {
            let data = try JSONEncoder().encode(self)
            guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {return nil}
            return dictionary
        } catch {return nil}
    }
}
