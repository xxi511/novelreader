//
//  ViewEX.swift
//  NovelReader
//
//  Created by 陳建佑 on 21/12/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    func setCorner(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setBorder(width: CGFloat=1, color: CGColor?=nil) {
        self.layer.borderWidth = width
        self.layer.borderColor = color
    }
}

