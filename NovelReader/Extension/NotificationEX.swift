//
//  NotificationEX.swift
//  NovelReader
//
//  Created by 陳建佑 on 22/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let NovelList = Notification.Name("NovelList")
    static let HomeList = Notification.Name("HomeList")
    static let NovelContent = Notification.Name("NovelContent")
    static let SearchResult = Notification.Name("SearchResult")
    static let ModifyData = Notification.Name("ModifyData")
    static let SendModify = Notification.Name("SendModify")
}
