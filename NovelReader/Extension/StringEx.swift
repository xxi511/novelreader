//
//  StringEx.swift
//  NovelReader
//
//  Created by 陳建佑 on 02/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    /// fetch string substring
    /// example
    /// str = "abcdefghi"
    /// str.substring(from: 0, to: 3) = "abcd"
    /// str.substring(from: 0, to: -1) = "abcdefghi"
    /// str.substring(from: 6, to: -13) = ""
    /// - Parameters:
    ///   - idx1: start index
    ///   - idx2: end index, accept negative number, same as python
    /// - Returns: the substring or "" if index error
    
    func substring(from idx1: Int, to idx2: Int) -> String {
        let len = self.count
        let _idx2 = (idx2 < 0) ? len + idx2: idx2
        let _idx1 = (idx1 < 0) ? len + idx1: idx1
        if _idx1 > _idx2 || _idx1 > len {
            return ""
        }
        
        let s = self.index(self.startIndex, offsetBy: _idx1)
        let e = self.index(self.startIndex, offsetBy: _idx2)
        return String(self[s...e])
    }
    
    func toMD5() -> String {
        let messageData = self.data(using: .utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func toCGFloat() -> CGFloat? {
        guard let dType = Double(self) else {return nil}
        return CGFloat(dType)
    }
    
    func replace(_ old: String, to new: String) -> String {
        return self.replacingOccurrences(of: old, with: new)
    }
}
