//
//  NavigationEX.swift
//  NovelReader
//
//  Created by 陳建佑 on 21/11/2017.
//  Copyright © 2017 陳建佑. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationItem {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.backBarButtonItem = backItem
    }}
